const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChoiceSchema = new Schema({
    choices: { type: String }
});

const QuestionSchema = new Schema({
    question: { type: String },
    choices: [ChoiceSchema],
    type: { type: String }
});

const Question = mongoose.model('Question', QuestionSchema);
module.exports = Question;