const express = require('express');
const router = express.Router();

const Question = require('../models/Question');

router.get('/', (req, res) => {
    Question.find(req.query)
        .then(question => res.status(200).send(question))
        .catch(error => res.status(500).send(error))
});

router.post('/', (req, res) => {
    Question.create({
        question: req.body.question,
        choices: req.body.choices,
        type: req.body.type
    })
    .then(() => res.status(200).send({success: true}))
    .catch((error) => res.status(500).send(error));
});


router.put('/:id', (req, res) => {
    Question.findByIdAndUpdate(req.params.id, req.body, {new: true})
        .then(() => res.status(200).send({success: true}))
        .catch(error => res.status(500).send(error))
});

router.delete('/:id', (req, res) => {
    Question.findByIdAndRemove(req.params.id)
        .then(() => res.status(200).send({success: true}))
        .catch(error => res.status(500).send(error))
});


module.exports = router;