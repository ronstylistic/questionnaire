const express = require('express');
const router = express.Router();
const Answer = require('../models/Answer');
const nodemailer = require('nodemailer');
const config = require('../config');
const hbs = require('nodemailer-express-handlebars');
const path = require('path');


router.post('/', (req, res) => {

    const transporter = nodemailer.createTransport({
      host: config.host,
      port: config.port,
      auth: {
        user: config.user,
        pass: config.pass
      }
    });

    transporter.use('compile', hbs({
      viewPath: path.resolve(__dirname, '../views'),
      extName: '.hbs'
    }));

    let mailOptions = {
      from: '"Ronnel S. Bedana" <ronnelbedana2011@gmail.com.com>', // sender address
      to: req.body.email, // list of receivers
      subject: 'Answer', // Subject line
      template: 'email',
      context: {
        answers: req.body.answers
      }
      //html: req.body.html // html body
    };    
    Answer.create({
        email: req.body.email,
        answers: req.body.answers
    })
    .then(() => {

        transporter.sendMail(mailOptions)
          .then((info) => {
              res.status(200).send({
                  success: true,
                  messageUrl: nodemailer.getTestMessageUrl(info)
              });
          })
          .catch((error) => res.status(500).send(error))
    })
    .catch(error => res.status(500).send(error))
});

module.exports = router;