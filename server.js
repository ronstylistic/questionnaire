const express = require('express');
const cors = require('cors');
const path = require('path');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const config = require('./server/config');

mongoose.connect(config.database, {
  useNewUrlParser: true,
  useCreateIndex: true
});

app.use(express.static(path.join(__dirname, 'dist')));

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(cors());

const Question = require('./server/controller/Question');
const Answer = require('./server/controller/Answer');

app.use('/api/v1/question', Question);
app.use('/api/v1/answer', Answer);

// Initialize the app.
const server = app.listen(process.env.PORT || 8080, function () {
  let port = server.address().port;
  console.log("App now running on port", port);
});