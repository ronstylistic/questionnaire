import { Component, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'ngbd-modal-email',
    template:
        `
    <div class="modal-header">
  <h4 class="modal-title">Provide Email</h4>
  <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
            <span aria-hidden="true">&times;</span>
        </button>
</div>
<form [formGroup]="myForm" (ngSubmit)="submitForm()">
    
  <div class="modal-body">
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" formControlName="email" required/>
      </div>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary rounded-0"
      [disabled]="!myForm.valid">
      Send
    </button>
  </div>
</form>
    `,
})
export class NgbdModalEmail {

    myForm: FormGroup;

    constructor(public activeModal: NgbActiveModal, 
        private fb: FormBuilder) {

        this.myForm = this.fb.group({
            email: [null, Validators.required],
        });    
    }

    submitForm(){
        this.activeModal.close(this.myForm.value);
    }
}
