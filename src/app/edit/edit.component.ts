import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../service/question.service';
import { AlertService } from '../service/alert.service';

enum action {
  submit,
  update
}

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})

export class EditComponent implements OnInit {

  public textType:boolean = true;
  public action:any = action.submit;
  public questions:any = [];

  public model:any = {};

  constructor(public questionService: QuestionService,
    public alert: AlertService) {
  }

  addChoices() {
    this.model.choices.push({choices: ""});
  }

  ngOnInit() {
    this.clearModel();
    this.getdata();
  }

  getdata(){
    let type = this.textType ? 'text' : 'selection';
    this.questionService.getQuestions({type: type})
      .subscribe((question) => {
        console.log('data', JSON.stringify(question, null, 2));
        this.questions = question;
      });
  }

  setSelectionType(type) {
    this.textType = type;
    this.action = action.submit;
    this.clearModel();
    this.getdata();
  }

  editQuestion(question) {
    this.model = question;
    this.action = action.update;
  }

  onRemove(question){
    
    if( this.action == action.update && question._id != null ){
      this.questionService.delete(question._id)
        .subscribe(
          (data) => {
            if(data){
              this.alert.success("You successfully deleted the question");
              this.questions.splice(question, 1);
              this.clearModel();
              this.action = action.submit;
            }
          },
          (error) => {
            this.alert.error(error);
          })
    }
    
  }

  onSubmitTextType(){
    
    let toSubmit = {
      question: this.model.question,
      choices: this.textType ? [] : this.model.choices,
      type: this.textType ? 'text' : 'selection'
    }
    
    if (this.action == action.submit) {
      this.questionService.create(toSubmit)
        .subscribe(
          (data) => {
              if (data.success) {
                this.alert.success("You successfully saved the question");
                this.clearModel();
              }
            },
            (error) => {
              this.alert.error(error);
          });
    }
    else if (this.action == action.update) {
      this.questionService.update(this.model._id, this.model)
          .subscribe(
            (data) => {
              if (data.success) {
                this.alert.success("You successfully update the question");
                this.clearModel();
                this.action = action.submit;
              }
            },
            (error) => {
              this.alert.error(error);
            })
    }
    
  }

  onRemoveChoices(index){
    this.model.choices.splice(1, index);
  }

  clearModel(){
    this.model = {
      question: "",
      choices: [{ choices: "" }],
      type: ""
    };
  }

}
