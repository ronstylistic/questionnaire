import { Injectable } from '@angular/core';
import { ApiService } from '../service/api.service';

@Injectable()
export class AnswerService {

    constructor(public api: ApiService) { }

    create(answerInfo) {
        return this.api.post('answer', answerInfo).map(res => res);
    }

}
