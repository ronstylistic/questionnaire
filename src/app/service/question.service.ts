import { Injectable } from '@angular/core';
import { ApiService } from '../service/api.service';

@Injectable()
export class QuestionService {

  constructor(public api: ApiService) {}

  create(questionInfo){
    return this.api.post('question', questionInfo).map(res => res);
  }

  getQuestions(params?: any){
    return this.api.get('question', params).map(res => res);
  }

  update(id, questionInfo){
    return this.api.put(`question/${id}`, questionInfo).map(res => res);
  }

  delete(id){
    return this.api.delete(`question/${id}`).map(res => res);
  }
}
