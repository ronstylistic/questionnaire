import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../service/question.service';
import { AlertService } from '../service/alert.service';
import { AnswerService } from '../service/answer.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalEmail } from '../directive/email-modal.component';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.css']
})
export class AnswerComponent implements OnInit {

  public textType: boolean = false;
  public questions:any = [];
  public answers:any = {};
  public isAnsweredAll: boolean = false;

  constructor(public questionService: QuestionService, 
    public alert: AlertService, 
    private modalService: NgbModal,
    public answerService: AnswerService) { }

  ngOnInit() {
    this.getQuestions();
  }

  getQuestions() {
    this.questionService.getQuestions({})
      .subscribe((question) => {        
        console.log('data', JSON.stringify(question, null, 2));
        this.questions = question;
      });
  }

  onSubmitAnswer(){
    this.answers = {
      email: '',
      answers: []
    };

    this.isAnsweredAll = true;
    
    this.questions.forEach(element => {
      if( element.answer == null ){
        this.alert.error("Kindly answer all the questions and submit again.");
        this.isAnsweredAll = false;
      }
      else{
        this.answers.answers.push({ question: element._id, answer: element.answer });  
      }
        
    });

    if(this.isAnsweredAll) {
      const modalRef = this.modalService.open(NgbdModalEmail, { backdrop: 'static' });
      
      modalRef.result.then(result => {
        this.answers.email = result.email;
        this.answerService.create(this.answers)
          .subscribe(
            (data) => {
              console.log('Email Sent', JSON.stringify(data, null, 2));
              if (data.success) {
                this.alert.success("You successfully saved your answer");
                this.answers = {};
              }
            },
            (error) => {
              //this.alert.error(error);

              console.log(error);
            }
          );

      }, (reason) => {
        console.log(reason);
      });
    }
  }
}
