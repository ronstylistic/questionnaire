import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { EditComponent } from './edit/edit.component';
import { AnswerComponent } from './answer/answer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { QuestionService } from './service/question.service';
import { ApiService } from './service/api.service';
import { AlertComponent } from './directive/alert.component';
import { AlertService } from './service/alert.service';
import { AnswerService } from './service/answer.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbdModalEmail } from './directive/email-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    EditComponent,
    AnswerComponent,
    NavbarComponent,
    AlertComponent,
    NgbdModalEmail
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot()
  ],
  providers: [
    ApiService, 
    QuestionService,
    AlertService,
    AnswerService
  ],
  entryComponents: [
    NgbdModalEmail
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
